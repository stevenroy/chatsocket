var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var mongoose = require('mongoose');
//users = [];
//change users array to just be an object and rename users to be name
var name = {}; 
connections = [];

server.listen(process.env.PORT || 3000);
console.log('Server running...');

mongoose.connect('mongodb://localhost/chat', { useNewUrlParser: true }, function(err){
	if(err){
		console.log(err);
	} else {
		console.log('Connected to mongodb');
	}
});

let chatSchema = mongoose.Schema({
	user:{
		type: String,
	},
	msg:{
		type: String,
	},
	created:{
		type: Date, 
		default: Date.now}},
	{ versionKey: false });
 
let Chat = mongoose.model('Message', chatSchema);


//route
app.get('/', function (req, res){
	res.sendFile(__dirname + '/index.html');
});

//open connection socket
io.sockets.on('connection', function(socket){
	//limit chat
	//exec is the execute function and then can put the callback function
	//want to get most recent chat use sort
	var query = Chat.find({});
	query.sort('-created').limit(6).exec(function(err, docs){
		if (err) throw err;
		console.log('Sending old msgs!');
		socket.emit('load old messages', docs);
	});
	connections.push(socket); //add socket
	console.log('Connected: %s sockets connected', connections.length);

	
	socket.on('disconnect', function(data){
		if(!socket.username) return; //Disconnect, check if they have username
		delete name[socket.username];
		updateUsernames();
		connections.splice(connections.indexOf(socket), 1);
		console.log('Disconnected: %s sockets connected', connections.length);
	});

	//Send Message
	socket.on('send message', function(data, callback){
		var msg = data.trim();
		if (msg.substr(0, 3) === '/w '){

			//check to see this send message so it would be in the format /w  

			msg = msg.substr(3);
			var ind = msg.indexOf(' ');

			//if the index(ind) is not negative 1 (-1) then we can checkto see if the username is valid
			
			if (ind !== -1){
				var private = msg.substring(0, ind);
				var msg = msg.substring(ind + 1);

				/*that's the start of the message the character right after the space 
				and then all the way to the end should be the rest of the message
				*/

				//if private is in the name object

				if (private in name){
					//want to change font private and can do something slighty
					// difference with them in client side
					name[private].emit('private', {msg: msg, user: socket.username});
					console.log('private!');
				} else {
					callback('Error: Enter a valid user.')
				}
			} else {

				//Just add the message we want to diplay in the callback
				
				callback('Error! Please enter a message for your private chat')
			}
		} else {
			let newMsg = new Chat({
				msg : msg, 
				user : socket.username
			});
			newMsg.save(function(err){
				if (err) throw err;
				io.sockets.emit('new message', {msg: msg, user: socket.username}); //implemenet username
			});
		}
	});

	//New user
	socket.on('new user', function(data, callback){
		if (data in name){  
			callback(false); 
		} else {
		callback(true);
		socket.username = data;
		name[socket.username] = socket; //Using the username as the key and the save the socket
		updateUsernames();
	}
	});

	//update username
	function updateUsernames(){
		io.sockets.emit('get users', Object.keys(name));
	}
});


//The indexOf() method returns the position of the first occurrence of a specified value in a string.
